extends WindowDialog

var graph_node_owner = null
var locale_field_owner = null
onready var text_edit := $MarginContainer/TextEdit

onready var color := $HBoxContainer/color/button
onready var selectcolor := $HBoxContainer/color/ColorPickerButton

onready var ghost := $HBoxContainer/ghost
onready var matrix := $HBoxContainer/matrix
onready var italics := $HBoxContainer/italics
onready var bold := $HBoxContainer/bold

onready var pulse := $HBoxContainer/pulse/button
onready var pulsefreq := $HBoxContainer/pulse/freq
onready var pulseheight := $HBoxContainer/pulse/height

onready var shake := $HBoxContainer/shake/button
onready var shakerate := $HBoxContainer/shake/rate
onready var shakelevel := $HBoxContainer/shake/level

onready var fade := $HBoxContainer/fade/button
onready var fadelength := $HBoxContainer/fade/length
onready var fadestart := $HBoxContainer/fade/start

onready var rainbow := $HBoxContainer/rainbow/button
onready var rainbowfreq := $HBoxContainer/rainbow/freq

onready var hide := $HBoxContainer/hide/button
onready var hidechar := $HBoxContainer/hide/char

onready var tornado := $HBoxContainer/tornado/button
onready var tornadoradius := $HBoxContainer/tornado/radius
onready var tornadofreq := $HBoxContainer/tornado/freq

func _ready() -> void:
  Events.connect("expand_text_dialogued_opened", self, "_on_Expand_text_dialogued_opened")
  text_edit.connect("text_changed", self, "_on_Text_changed")
  italics.connect("pressed", self, "_on_italics_pressed") #done
  bold.connect("pressed", self, "_on_bold_pressed")       #done
  color.connect("pressed", self, "_on_color_pressed")     #done
  ghost.connect("pressed", self, "_on_ghost_pressed")     #done
  matrix.connect("pressed", self, "_on_matrix_pressed")   #done
  pulse.connect("pressed", self, "_on_pulse_pressed")     #done
  shake.connect("pressed", self, "_on_shake_pressed")
  fade.connect("pressed", self, "_on_fade_pressed")
  rainbow.connect("pressed", self, "_on_rainbow_pressed")
  hide.connect("pressed", self, "_on_hide_pressed")
  tornado.connect("pressed", self, "_on_tornado_pressed")

func _on_Expand_text_dialogued_opened(field_owner) -> void:
  graph_node_owner = field_owner.owner
  locale_field_owner = field_owner
  window_title = graph_node_owner.uuid
  text_edit.text = graph_node_owner.values.data.text[Editor.locale]
  popup()

func _on_Text_changed() -> void:
  graph_node_owner.values.data.text[Editor.locale] = text_edit.text
  locale_field_owner.locale.text = text_edit.text
  
func _on_pulse_pressed() -> void:
  text_edit.insert_text_at_cursor("[pulse height=%s freq=%s color=%s]" % [pulseheight.value, pulsefreq.value, selectcolor.color.to_html()] + text_edit.get_selection_text() + "[/pulse]")

func _on_color_pressed() -> void:
  text_edit.insert_text_at_cursor("[color=%s]" % selectcolor.color.to_html() + text_edit.get_selection_text() + "[/color]")

func _on_matrix_pressed() -> void:
  text_edit.insert_text_at_cursor("[matrix]" + text_edit.get_selection_text() + "[/matrix]")

func _on_ghost_pressed() -> void:
  text_edit.insert_text_at_cursor("[ghost]" + text_edit.get_selection_text() + "[/ghost]")

func _on_bold_pressed() -> void:
  text_edit.insert_text_at_cursor("[b]" + text_edit.get_selection_text() + "[/b]")

func _on_italics_pressed() -> void:
  text_edit.insert_text_at_cursor("[i]" + text_edit.get_selection_text() + "[/i]")

func _on_shake_pressed() -> void:
  text_edit.insert_text_at_cursor("[shake rate=%s level=%s]" % [shakerate.value, shakelevel.value] + text_edit.get_selection_text() + "[/shake]")

func _on_fade_pressed() -> void:
  text_edit.insert_text_at_cursor("[fade start=%s length=%s]" % [fadestart.value, fadelength.value] + text_edit.get_selection_text() + "[/fade]")

func _on_rainbow_pressed() -> void:
  text_edit.insert_text_at_cursor("[rainbow freq=%s]" % [rainbowfreq.value] + text_edit.get_selection_text() + "[/rainbow]")

func _on_hide_pressed() -> void:
  text_edit.insert_text_at_cursor("[hide char=%s]" % [hidechar.text] + text_edit.get_selection_text() + "[/hide]")

func _on_tornado_pressed() -> void:
  text_edit.insert_text_at_cursor("[tornado radius=%s freq=%s]" % [tornadoradius.value, tornadofreq.value] + text_edit.get_selection_text() + "[/tornado]")
