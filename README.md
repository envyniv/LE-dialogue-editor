![banner](./packages/banner.jpg)

*LE Dialogue Editor* is a standalone application, made with the [Godot Engine](https://godotengine.org/), created with the main purpose to manage tiny but complex dialogues.

Learn more in the [WIKI](https://github.com/Levrault/levrault-dialogue-editor/wiki)

## Credits

- Icons : https://feathericons.com/
- uuid : https://github.com/binogure-studio/godot-uuid/blob/master/uuid.gd

